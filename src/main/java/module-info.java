module org.ssldev.api.sslapi {
//	exports org.ssldev.api.utils;
	exports org.ssldev.api; 
	exports org.ssldev.api.gui; 
	exports org.ssldev.api.messages;
	exports org.ssldev.api.services;
//	exports org.ssldev.api.fields;
	exports org.ssldev.api.app;
	exports org.ssldev.api.chunks;
	
//	exports org.ssldev.api.gui to javafx.graphics;

	opens org.ssldev.api.gui to javafx.fxml;
	
	requires transitive org.ssldev.core;
	
	requires javafx.controls;
	requires javafx.fxml; 
}