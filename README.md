SSLAPI
======

the SSL API parses in the binary history file produced by Serato DJ / ScratchLive
and publishes out play data as events.  It is meant to act as a middleware and
serve as an (unofficial) API for any developers looking to utilize Serato play data.  

This project was inspired by Ben XO’s SslScrobbler project.  He has some great 
documentation on the SSL binary format and overall chunk structure, so i won’t 
repeat it here.  Check out his project for more details: https://github.com/ben-xo/sslscrobbler

contact me via __http://projects.ssldev.org__ with any comments/questions.



Dev Notes:
----------

**v0.4-SNAPSHOT:** 
* updated to support java 11+

**v0.3:** 
* supports Serato DJ Pro v2.3.2
* adds ability to decode/encode the serato DB file
* stability improvements & bug fixes
* last version to support **java 8**



Maven:
------
Add SSL-API to your Maven project (use v0.3 for java 8):

```xml
  <dependency>
   <groupId>org.ssldev</groupId>
   <artifactId>SSL-API</artifactId>
   <version>0.4-SNAPSHOT</version> 
   <scope>compile</scope>
  </dependency>
```


Working in Eclipse (`v0.3` supports JDK 8)(`v0.4` supports JDK 11)
-----------------------------------------
You may need to download the latest `e(fx)clipse` plugin from the eclipse market place,
to avoid JAVAFX compilation warnings.


projects depending on SSL-API should include the dependency in their module-info files:

```java
requires org.ssldev.api.sslapi;
```


Quick Start (using maven)
-----------------
1. clone the repo:   
   * git clone git@gitlab.com:eladmaz/SSL-API.git
2. build the project:   
   * cd SSL-API/
   * mvn clean install
3. start the API to view real-time play events from Serato DJ (in terminal)
   * mvn exec:java

I created a GUI to demonstrate the type of play data that can be received:
1. set `startGui=true` in `.../ssl-api/src/main/resources/sslApiConfiguration.properties`

The GUI can be started (`mvn exec:java`) before or after SSL/Serato DJ, and should display a track
as soon as it is loaded to one of the decks.  The ssldom.txt file (`/home/<user>/.sslapi/ssldom.txt`)
will capture all tracks played so far in the current session.  Logging is piped 
to `/home/<user>/.sslapi/log.txt`.


How to use the API
------------------
The recommended way to use SSLAPI is to simply register a service client and specify
which messages the client should receive (see `org.ssldev.api.demo.UsageExample.java`):

```java
		/*
		 * 1. instantiate the ssl-api 
		 */
		SslApi api = new SslApi();
		
		
		/*
		 * 2. register a service that will simply print out incoming tracks loaded/ejected.
		 */
		api.register( msg -> System.out.println("DEMO APP got notified of: " + msg), 
					  // subscribe for track loading and ejecting notifications 
					  TrackLoadedMessage.class, TrackUnloadedMessage.class );
		
		/*
		 * 3. start the API.  The API will start listening for Serato session 
		 * file changes, and publish out play data as events (e.g. TrackLoadedMessage)
		 */
		api.start();
```

How does it work
----------------
the app framework is designed around the EventHub pub/sub pattern, where each service 
registers with a central event hub, and acts upon message/events that it is interested
in.  An async event hub is used (i.e blocking queue) as a simplistic threading 
model for the app.

once initialized, SSLAPI picks out which history session file to monitor (if Serato DJ/SSL 
has started; otherwise, it'll wait for one to be created)(`SslCurrentSessionFinderService`).
Then on each consecutive update it parses out newly appended bytes into OENT/ADAT
chunks (`SslByteConsumerService` & `DataConsumedPublisherService`).

Byte Consumers fall into 2 types: compound & leafs.  Compound consumers (e.g OENT, ADAT)
are made up of child byte consumers.  Each byte consumer encapsulates all knowledge
of how to consume binary data and store it.

The intent of the design is to make it easy to add/modify/edit the byte consumers
as the SSL binary format evolves.  For example, the ADAT chunk contains data fields
such as BPM, TITLE, etc.  Adding a new field is as easy as creating a new field
byte consumer specifying what consumption strategy to use, and adding it to the 
ADAT via the `constructAdat()` method.    

Messages/Events
---------------
To provide a more meaningful abstraction, the API models “decks” that publish 
events such as ‘NowPlaying’ or ‘NowInCue’ to show what tracks are playing and on
what deck.  In contrast, non-abstracted data is contained in the AdatConsumedMessage 

NOTE: since SeratoDj/SSL cannot differentiate whether a song is really playing 
or not (controlled by the mixer crossfader/volume knob) neither can this API.  To 
keep things simple, no effort was taken to guess when a track is actually playing 
or not.  At the moment it's left up to client applications to implement the logic 
to do so.  


Projects using SSL-API:
-----------------------
* [Serato Crate Exporter](https://gitlab.com/eladmaz/SeratoCrateExporter)
* [Serato Play Counter](https://gitlab.com/eladmaz/serato-play-counter)


Disclaimer
----------
SSLAPI is free open source software licensed under the MIT license and is provided 
without warranties of any kind.
